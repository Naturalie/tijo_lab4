package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class CreateMovieMapper {

    public Movie dtoToMovie(CreateMovieDto movie){

        Movie newMovie = new Movie();

        //newMovie.setMovieId(); NIE USTAWIAC
        newMovie.setImage(movie.getImage());
        newMovie.setTitle(movie.getTitle());
        newMovie.setYear(movie.getYear());

        return newMovie;
    }
}
